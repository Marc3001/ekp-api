from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from ekp.dependencies import get_db
from ekp.schemas.hosting import HostingCreation, HostingResult
from ekp.services.hosting import HostingService
from ekp.workers.hosting import create_hosting_task, delete_hosting_task

router = APIRouter(
    prefix="/hosting",
    tags=["hosting"],
)


@router.get("/")
async def list_hostings(db: Session = Depends(get_db)) -> list[HostingResult]:
    hostings = HostingService(db).list_hostings()
    return hostings


@router.get("/{hosting_id}")
async def get_hosting(hosting_id: int, db: Session = Depends(get_db)) -> HostingResult:
    hosting = HostingService(db).get_hosting(hosting_id)
    if hosting is None:
        raise HTTPException(status_code=404, detail=f"Hosting does not exist")
    return hosting


@router.post("/")
async def create_hosting(content: HostingCreation, db: Session = Depends(get_db)):
    hosting = HostingService(db).pre_create_hosting(content)
    create_hosting_task.delay({"id": hosting.id})
    return hosting


@router.delete("/{hosting_id}")
async def delete_hosting(hosting_id: int, db: Session = Depends(get_db)):
    HostingService(db).pre_delete_hosting(hosting_id)
    delete_hosting_task.delay({"id": hosting_id})
    return {"status": "deletion in progress"}
