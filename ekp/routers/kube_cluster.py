from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from ekp.dependencies import get_db
from ekp.schemas.kube_cluster import KubeClusterCreation, KubeClusterResult
from ekp.schemas.node import Node
from ekp.services.kube_cluster import KubeClusterService
from ekp.workers.kube_cluster import create_kube_cluster_task, delete_kube_cluster_task

router = APIRouter(
    prefix="/kube_cluster",
    tags=["kubernetes"],
)


@router.get("/")
async def list_clusters(db: Session = Depends(get_db)) -> list[KubeClusterResult]:
    return KubeClusterService(db).list_kube_clusters()


@router.get("/{cluster_id}")
async def get_cluster(
    cluster_id: int, db: Session = Depends(get_db)
) -> KubeClusterResult:
    cluster = KubeClusterService(db).get_kube_cluster(cluster_id)
    if cluster is None:
        raise HTTPException(status_code=404, detail=f"Cluster does not exist")
    return cluster


@router.post("/")
async def create_cluster(content: KubeClusterCreation, db: Session = Depends(get_db)):
    cluster = KubeClusterService(db).pre_create_kube_cluster(content)
    create_kube_cluster_task.delay({"id": cluster.id})
    return cluster


@router.delete("/{cluster_id}")
async def delete_cluster(cluster_id: int, db: Session = Depends(get_db)):
    cluster = KubeClusterService(db).pre_delete_kube_cluster(cluster_id)
    task = delete_kube_cluster_task.delay({"id": cluster_id})
    return {"status": "deletion in progress"}


@router.get("/{cluster_id}/node")
async def get_cluster(cluster_id: int, db: Session = Depends(get_db)) -> list[Node]:
    cluster = KubeClusterService(db).get_kube_cluster(cluster_id)
    if cluster is None:
        raise HTTPException(status_code=404, detail=f"Cluster does not exist")
    return cluster.nodes
