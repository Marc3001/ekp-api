import os
from celery import Celery
from sqlalchemy import create_engine

celery_app = Celery(__name__)
celery_app.conf.broker_url = os.environ.get(
    "CELERY_BROKER_URL", "redis://localhost:6379"
)
celery_app.conf.result_backend = os.environ.get(
    "CELERY_RESULT_BACKEND", "redis://localhost:6379"
)

celery_app.autodiscover_tasks(
    [
        "ekp.workers.kube_cluster",
        "ekp.workers.hosting",
    ],
    force=True,
)
