from typing import List
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Enum, Table
from sqlalchemy.orm import relationship, mapped_column, Mapped
from ekp.database import Base


class Node(Base):
    __tablename__ = "node"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    pve_vmid = Column(Integer)
    pve_node = Column(String)
    pve_cloudinit_image = Column(String)
    tags = Column(String)
    ip = Column(String)
    master = Column(Boolean)
    hosting_id: Mapped[int] = mapped_column(ForeignKey("hosting.id"))
    hosting: Mapped["Hosting"] = relationship(
        back_populates="nodes",
    )
    kube_cluster_id: Mapped[int] = mapped_column(ForeignKey("kube_cluster.id"))
    kube_cluster: Mapped["KubeCluster"] = relationship(
        back_populates="nodes",
    )
