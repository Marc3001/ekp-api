import enum
from typing import List
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Enum, Table
from sqlalchemy.orm import relationship, Mapped, mapped_column
from ekp.database import Base


class KubeClusterStatus(str, enum.Enum):
    creation_in_progress = "CREATION_IN_PROGRESS"
    not_ready = "NOT_READY"
    ready = "READY"
    deletion_in_progress = "DELETION_IN_PROGRESS"


kube_cluster_hosting_ass_table = Table(
    "kube_cluster_hosting",
    Base.metadata,
    Column(
        "kube_cluster",
        ForeignKey("kube_cluster.id", ondelete="CASCADE"),
        primary_key=True,
    ),
    Column("hosting", ForeignKey("hosting.id", ondelete="CASCADE"), primary_key=True),
)


class KubeClusterFlavor(str, enum.Enum):
    k3s = "k3s"


class KubeClusterTokenType(str, enum.Enum):
    server = "server"
    agent = "agent"


class KubeClusterToken(Base):
    __tablename__ = "kube_cluster_token"

    id = Column(Integer, primary_key=True)
    token = Column(String)
    token_type = Column(Enum(KubeClusterTokenType))
    kube_cluster_id: Mapped[int] = mapped_column(ForeignKey("kube_cluster.id"))
    kube_cluster: Mapped["KubeCluster"] = relationship(
        back_populates="tokens",
    )


class KubeCluster(Base):
    __tablename__ = "kube_cluster"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    status = Column(Enum(KubeClusterStatus))
    flavor = Column(Enum(KubeClusterFlavor))
    version = Column(String)
    region = Column(String)
    hostings: Mapped[List["Hosting"]] = relationship(
        secondary=kube_cluster_hosting_ass_table,
        back_populates="kube_clusters",
        cascade="all, delete",
        passive_deletes=True,
    )
    nodes: Mapped[List["Node"]] = relationship(
        back_populates="kube_cluster",
    )
    tokens: Mapped[List["KubeClusterToken"]] = relationship(
        back_populates="kube_cluster",
    )
    services_subnet = Column(String)
    pods_subnet = Column(String)
    management_ip = Column(String)
    loadbalancer_ips = Column(String)
