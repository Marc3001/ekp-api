import enum
from typing import List
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, Enum, Table, ARRAY
from sqlalchemy.orm import relationship, mapped_column, Mapped
from ekp.database import Base
from ekp.models.kube_cluster import kube_cluster_hosting_ass_table


class HostingType(enum.Enum):
    proxmox = "proxmox"


class HostingStatus(enum.Enum):
    creation_in_progress = "CREATION_IN_PROGRESS"
    ready = "READY"
    deletion_in_progress = "DELETION_IN_PROGRESS"
    error = "ERROR"


class Hosting(Base):
    __tablename__ = "hosting"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, index=True)
    status = Column(Enum(HostingStatus))
    hosting_type = Column(Enum(HostingType))
    cluster_url = Column(String)
    pve_token_name = Column(String)
    pve_token_value = Column(String)
    pve_user = Column(String)
    resource_group_name = Column(String)
    region_name = Column(String)
    zone_name = Column(String)
    network_bridge_name = Column(String)
    nodes_subnet = Column(String)
    nodes_gateway = Column(String)
    nodes_dns_servers = Column(ARRAY(String))
    vlan_id = Column(Integer, nullable=True)
    images_path = Column(String)
    kube_clusters: Mapped[List["KubeCluster"]] = relationship(
        secondary=kube_cluster_hosting_ass_table,
        back_populates="hostings",
        cascade="all, delete",
        passive_deletes=True,
    )
    nodes: Mapped[List["Node"]] = relationship(
        back_populates="hosting",
    )
