from sqlalchemy.orm import Session
from ekp.services.hosting import HostingService
from ekp.worker import celery_app
from ekp.database import engine


@celery_app.task(name="create_hosting")
def create_hosting_task(hosting_id):
    with Session(engine) as session:
        HostingService(session).create_hosting(hosting_id)
    return True


@celery_app.task(name="delete_hosting")
def delete_hosting_task(hosting_id):
    with Session(engine) as session:
        HostingService(session).delete_hosting(hosting_id)
    return True
