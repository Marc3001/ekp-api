from sqlalchemy.orm import Session
from ekp.services.kube_cluster import KubeClusterService
from ekp.worker import celery_app
from ekp.database import engine


@celery_app.task(name="create_kube_cluster")
def create_kube_cluster_task(cluster_id):
    with Session(engine) as session:
        KubeClusterService(session).create_kube_cluster(cluster_id)
    return True


@celery_app.task(name="delete_kube_cluster")
def delete_kube_cluster_task(cluster_id):
    with Session(engine) as session:
        KubeClusterService(session).delete_kube_cluster(cluster_id)
    return True
