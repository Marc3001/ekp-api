from pydantic import BaseModel
from ekp.schemas.kube_cluster import KubeClusterBase
from ekp.schemas.node import Node


class HostingCreation(BaseModel):
    name: str
    cluster_url: str
    pve_token_name: str
    pve_token_value: str
    pve_user: str
    resource_group_name: str
    region_name: str
    zone_name: str
    network_bridge_name: str
    nodes_subnet: str
    nodes_gateway: str
    nodes_dns_servers: list[str]
    vlan_id: int | None = None
    images_path: str


class HostingResult(BaseModel):
    id: int
    status: str
    name: str
    cluster_url: str
    pve_token_name: str
    pve_user: str
    resource_group_name: str
    region_name: str
    zone_name: str
    network_bridge_name: str
    nodes_subnet: str
    nodes_gateway: str
    nodes_dns_servers: list[str]
    vlan_id: int | None = None
    images_path: str
    kube_clusters: list[KubeClusterBase]
    nodes: list[Node]
