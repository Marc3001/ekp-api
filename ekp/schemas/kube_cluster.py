from enum import Enum
from pydantic import BaseModel, Field, computed_field


class KubeClusterFlavor(str, Enum):
    k3s = "k3s"


class KubeClusterBase(BaseModel):
    version: str
    region: str
    name: str
    flavor: KubeClusterFlavor = KubeClusterFlavor.k3s
    services_subnet: str
    pods_subnet: str
    management_ip: str
    loadbalancer_ips: str


class KubeClusterCreation(KubeClusterBase):
    hostings: list[int]


class KubeClusterHosting(BaseModel):
    id: int


class KubeClusterResult(KubeClusterBase):
    id: int
    status: str
    hostings: list[KubeClusterHosting]
