from pydantic import BaseModel


class Node(BaseModel):
    name: str
    tags: str
    master: bool
    pve_vmid: int | None
    pve_node: str | None
    pve_cloudinit_image: str | None
    ip: str | None
    hosting_id: int
