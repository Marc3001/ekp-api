from ekp.routers import auth, hosting, kube_cluster
from ekp.database import Base, engine, drop_everything
from fastapi import FastAPI
from pydantic_settings import BaseSettings

summary = """
    Easy Kubernetes on Proxmox
"""

description = """
    API which will manage kubernetes clusters hosted on proxmox.
    Will manage kubernetes completely, poping nodes if needed and upgrading them
"""


class Settings(BaseSettings):
    secret: str = "ChangeMe"


settings = Settings()

# drop_everything(engine)
# Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind=engine)

app = FastAPI(
    title="Easy Kubernetes on Proxmox",
    summary=summary,
    description=description,
)

app.include_router(auth.router)
app.include_router(hosting.router)
app.include_router(kube_cluster.router)
