from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.orm import Session
from ekp.models.hosting import Hosting, HostingType, HostingStatus
from ekp.services.proxmox import ProxmoxService


class HostingService:
    def __init__(self, db: Session):
        self.db = db

    def pre_create_hosting(self, obj):
        hosting_exists_stmt = select(Hosting).where(Hosting.name == obj.name)
        hosting_exists = self.db.scalar(hosting_exists_stmt)
        if hosting_exists is not None:
            raise HTTPException(
                status_code=409, detail=f"Hosting with name {obj.name} already exists"
            )
        hosting = Hosting(
            name=obj.name,
            status=HostingStatus.creation_in_progress,
            hosting_type=HostingType.proxmox,
            cluster_url=obj.cluster_url,
            pve_token_name=obj.pve_token_name,
            pve_token_value=obj.pve_token_value,
            pve_user=obj.pve_user,
            resource_group_name=obj.resource_group_name,
            region_name=obj.region_name,
            zone_name=obj.zone_name,
            network_bridge_name=obj.network_bridge_name,
            nodes_subnet=obj.nodes_subnet,
            nodes_gateway=obj.nodes_gateway,
            nodes_dns_servers=obj.nodes_dns_servers,
            vlan_id=obj.vlan_id,
            images_path=obj.images_path,
        )
        self.db.add(hosting)
        self.db.commit()
        return hosting

    def create_hosting(self, hosting_id):
        hosting = self.get_hosting(hosting_id)
        try:
            ProxmoxService(hosting).test_connection()
            hosting.status = HostingStatus.ready
        except Exception as ex:
            hosting.status = HostingStatus.error
            raise ex
        self.db.flush()
        self.db.commit()

    def get_hosting(self, hosting_id):
        return self.db.get(Hosting, hosting_id)

    def list_hostings(self):
        return self.db.scalars(select(Hosting)).all()

    def pre_delete_hosting(self, hosting_id):
        hosting = self.db.get(Hosting, hosting_id)
        if hosting is None:
            raise HTTPException(status_code=404, detail=f"Hosting does not exist")
        if len(hosting.nodes) > 0:
            raise HTTPException(status_code=400, detail=f"Cannot delete actually used hosting")
        hosting.status = HostingStatus.deletion_in_progress
        self.db.commit()
        self.db.flush()

    def delete_hosting(self, hosting_id):
        hosting = self.get_hosting(hosting_id)
        self.db.delete(hosting)
        self.db.commit()
