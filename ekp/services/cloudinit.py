from io import BytesIO
import random
import string
import pycdlib


class CloudInitService:
    def generate_iso(self, filepath, content={}):
        """
        Will create iso file from files and their content provided
        """
        iso = pycdlib.PyCdlib()
        iso.new(rock_ridge="1.09", joliet=3, vol_ident="cidata")
        for file in content:
            iso.add_fp(
                BytesIO(bytes(content[file], "utf8")),
                len(content[file]),
                f"/{''.join(random.choices(string.ascii_uppercase, k=6))}.;1",
                rr_name=file,
                joliet_path=f"/{file}",
            )
        iso.write(filepath)
        iso.close()
