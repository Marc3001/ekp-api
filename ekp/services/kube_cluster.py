import random
import string
from fastapi import HTTPException
from sqlalchemy import select
from sqlalchemy.orm import Session
from ekp.models.kube_cluster import (
    KubeCluster,
    KubeClusterStatus,
    KubeClusterToken,
    KubeClusterTokenType,
)
from ekp.models.node import Node
from ekp.services.hosting import HostingService
from ekp.services.proxmox import ProxmoxService


class KubeClusterService:
    def __init__(self, db: Session):
        self.db = db

    def pre_create_kube_cluster(self, obj):
        cluster_exists_stmt = select(KubeCluster).where(KubeCluster.name == obj.name)
        cluster_exists = self.db.scalar(cluster_exists_stmt)
        if cluster_exists is not None:
            raise HTTPException(
                status_code=409, detail=f"Cluster with name {obj.name} already exists"
            )
        hostings_obj = [HostingService(self.db).get_hosting(i) for i in obj.hostings]
        cluster = KubeCluster(
            name=obj.name,
            status=KubeClusterStatus.creation_in_progress,
            region=obj.region,
            version=obj.version,
            flavor=obj.flavor,
            hostings=hostings_obj,
            pods_subnet=obj.pods_subnet,
            services_subnet=obj.services_subnet,
            management_ip=obj.management_ip,
            loadbalancer_ips=obj.loadbalancer_ips,
        )
        self.db.add(cluster)
        self.db.commit()
        return cluster

    def create_kube_cluster_token(self):
        return "".join(random.choices(string.ascii_lowercase, k=24))

    def create_kube_cluster(self, kube_cluster_id):
        cluster = self.get_kube_cluster(kube_cluster_id)

        # Create first manager token
        token = KubeClusterToken(
            token=self.create_kube_cluster_token(),
            token_type=KubeClusterTokenType.server,
            kube_cluster=cluster,
        )
        self.db.add(token)
        self.db.commit()
        self.db.flush()

        if len(cluster.hostings) == 0:
            cluster.status = KubeClusterStatus.not_ready
        else:
            first_ip = None
            for i in range(min(len(cluster.hostings), 3)):
                hosting_id = cluster.hostings[i].id
                creation_parameters = {
                    "hosting": cluster.hostings[i],
                    "type": "manager",
                    "kube_cluster": cluster,
                    "additional_conf": {
                        "k3s_token": token.token,
                        "services_subnet": cluster.services_subnet,
                        "pods_subnet": cluster.pods_subnet,
                    },
                }
                if first_ip is not None:
                    creation_parameters["additional_conf"][
                        "master_url"
                    ] = f"https://{first_ip}:6443"
                else:
                    creation_parameters["additional_conf"][
                        "management_ip"
                    ] = cluster.management_ip
                    creation_parameters["additional_conf"][
                        "loadbalancer_ips"
                    ] = [cluster.loadbalancer_ips]
                node = self.create_node(**creation_parameters)
                if first_ip is None:
                    first_ip = node.ip
            cluster.status = KubeClusterStatus.ready
        self.db.commit()
        self.db.flush()

    def get_kube_cluster(self, kube_cluster_id):
        return self.db.get(KubeCluster, kube_cluster_id)

    def list_kube_clusters(self):
        return self.db.scalars(select(KubeCluster)).all()

    def pre_delete_kube_cluster(self, kube_cluster_id):
        kube_cluster = self.get_kube_cluster(kube_cluster_id)
        if kube_cluster is None:
            raise HTTPException(status_code=404, detail=f"KubeCluster does not exist")
        kube_cluster.status = KubeClusterStatus.deletion_in_progress
        self.db.commit()
        self.db.flush()

    def delete_kube_cluster(self, kube_cluster_id):
        kube_cluster = self.get_kube_cluster(kube_cluster_id)
        for token in kube_cluster.tokens:
            self.db.delete(token)
        for node in kube_cluster.nodes:
            self.delete_node(node.id)
        self.db.delete(kube_cluster)
        self.db.commit()

    def get_node(self, node_id):
        return self.db.get(Node, node_id)

    def create_node(self, hosting, kube_cluster, type, additional_conf={}):
        node = Node(
            name=f"{kube_cluster.name}-{type}-{''.join(random.choices(string.ascii_lowercase, k=6))}",
            tags=",".join(["ekp", type, hosting.zone_name]),
            master=type == "manager",
            hosting_id=hosting.id,
            kube_cluster_id=kube_cluster.id,
        )
        self.db.add(node)
        self.db.commit()
        ProxmoxService(hosting).create_node(
            node_obj=node,
            additional_conf=additional_conf,
        )
        self.db.commit()
        self.db.flush()
        return node

    def delete_node(self, node_id):
        node = self.get_node(node_id)
        if node.pve_vmid is not None:
            ProxmoxService(node.hosting).delete_node(
                node_obj=node,
            )
        self.db.delete(node)
        self.db.commit()
