import ipaddress
import os.path
import random
import re
import requests
from ekp.services.cloudinit import CloudInitService
from ekp.services.proxmox_api import (
    ProxmoxApiAuth,
    ProxmoxApiService,
    ProxmoxApiVmDoesNotExistException,
)


class EkpImageNotFound(Exception):
    pass


class ProxmoxService:
    def __init__(self, hosting_obj):
        self.hosting_obj = hosting_obj
        self.proxmox_auth = ProxmoxApiAuth(
            host=hosting_obj.cluster_url,
            user=hosting_obj.pve_user,
            token_name=hosting_obj.pve_token_name,
            token_value=hosting_obj.pve_token_value,
            verify_ssl=False,
        )

    def test_connection(self):
        ProxmoxApiService(self.proxmox_auth).list_nodes()

    def get_ekp_last_image(self, cluster_version, cluster_flavor):
        r = requests.get(
            "https://ekp-images-mcesarine-ekp-186c26bb60a2dd32f9e3aa827d5872f8bfd896.gitlab.io/images.json"
        )
        json = r.json()
        image_array = [i for i in json if i["name"] == f"ekp-{cluster_flavor}-u22.img"]
        if len(image_array) == 0:
            raise EkpImageNotFound
        result = image_array[0]
        result["local_name"] = (
            f"ekp-{cluster_flavor}-u22-{image_array[0]['checksums']['sha1'][:6]}.qcow2"
        )
        return result

    def download_ekp_image(
        self, pve_node, cluster_version, cluster_flavor, images_path
    ):
        last_available_image = self.get_ekp_last_image(cluster_version, cluster_flavor)
        image_name = last_available_image["local_name"]
        image_relative_path = os.path.join("10000", image_name)
        image_local_path = os.path.join(images_path, image_relative_path)
        if not os.path.isfile(image_local_path):
            if not os.path.exists(os.path.dirname(image_local_path)):
                print(f"Creating folder {os.path.dirname(image_local_path)}")
                os.makedirs(os.path.dirname(image_local_path))
            print("Downloading ekp image...")
            image_url = last_available_image["url"]
            r = requests.get(image_url)
            with open(image_local_path, "wb") as fd:
                fd.write(r.content)
            print("Done")

        image_id = f"local-nfs:{image_relative_path}"
        return image_id

    def get_hosting_freeip(self):
        used_ips = [str(node.ip) for node in self.hosting_obj.nodes]
        free_ips = [
            ip
            for ip in ipaddress.IPv4Network(self.hosting_obj.nodes_subnet).hosts()
            if str(ip) not in used_ips
        ]
        return random.choice(free_ips)

    def create_node(self, node_obj, additional_conf={}):
        pve_nodes = ProxmoxApiService(self.proxmox_auth).list_nodes()
        chosen_pve_node = random.choice(pve_nodes)

        # Upload vm image if does not already exists
        image_id = self.download_ekp_image(
            chosen_pve_node["node"],
            node_obj.kube_cluster.version,
            node_obj.kube_cluster.flavor.value,
            self.hosting_obj.images_path,
        )

        used_vmids = [
            v["vmid"]
            for v in ProxmoxApiService(self.proxmox_auth).list_vms(
                node=chosen_pve_node["node"]
            )
        ] + [
            v["vmid"]
            for v in ProxmoxApiService(self.proxmox_auth).list_containers(
                node=chosen_pve_node["node"]
            )
        ]
        next_id = random.choices(list(set(range(1000, 1200)) - set(used_vmids)))[0]

        # Create vm
        net_conf = f"model=virtio,bridge={self.hosting_obj.network_bridge_name}"
        if self.hosting_obj.vlan_id is not None:
            net_conf += f",tag={self.hosting_obj.vlan_id}"
        ProxmoxApiService(self.proxmox_auth).create_vm(
            node=chosen_pve_node["node"],
            vmid=next_id,
            name=node_obj.name,
            pool=self.hosting_obj.resource_group_name,
            tags=node_obj.tags.split(","),
            net0=net_conf,
            memory="4096",
            vga="type=serial0",
            serial0="socket",
            scsi0=f"file=local-lvm:0,import-from={image_id}",
            scsihw="virtio-scsi-single",
            ostype="l26",
            agent="enabled=1,type=virtio",
        )
        node_obj.pve_vmid = next_id
        node_obj.pve_node = chosen_pve_node["node"]

        # Get free IP address
        node_ipaddress = self.get_hosting_freeip()
        node_netmask = ipaddress.IPv4Network(self.hosting_obj.nodes_subnet).netmask
        node_obj.ip = str(node_ipaddress)

        # Get vm mac addresses
        conf = ProxmoxApiService(self.proxmox_auth).get_vm_config(
            node=chosen_pve_node["node"], vmid=next_id
        )
        mac_match = re.match(
            r"virtio=([A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2}:[A-F0-9]{2})",
            conf["net0"],
        )
        mac = mac_match.group(1)

        # Get vm instance-id
        ci_meta = ProxmoxApiService(self.proxmox_auth).get_vm_cloudinit_dump(
            node=chosen_pve_node["node"],
            vmid=next_id,
            type="meta",
        )
        instanceid_match = re.match(r"instance-id: (.*)", ci_meta)
        instanceid = mac_match.group(1)

        # Generate cloud-init conf
        k3s_mode = "server" if node_obj.master else "agent"
        k3s_master_url = ""
        if "master_url" in additional_conf.keys():
            k3s_master_url = f"master_url: {additional_conf['master_url']}"
        k3s_token = additional_conf["k3s_token"]
        load_balancers_ips = ""
        management_ip = ""
        services_subnet = ""
        pods_subnet = ""
        if "loadbalancer_ips" in additional_conf.keys():
            load_balancers_ips = (
                f"load_balancers_ips: [{','.join(additional_conf['loadbalancer_ips'])}]"
            )
        if "management_ip" in additional_conf.keys():
            management_ip = f"management_ip: {additional_conf['management_ip']}"
        if "services_subnet" in additional_conf.keys():
            services_subnet = f"service_cidr: {additional_conf['services_subnet']}"
        if "pods_subnet" in additional_conf.keys():
            pods_subnet = f"cluster_cidr: {additional_conf['pods_subnet']}"
        filename = f"cloud-init-{next_id}.iso"
        cloudinit_local_path = f"/tmp/{filename}"
        CloudInitService().generate_iso(
            filepath=cloudinit_local_path,
            content={
                "network-config": f"""
version: 1
config:
    - type: physical
      name: eth0
      mac_address: '{mac.lower()}'
      subnets:
      - type: static
        address: '{str(node_ipaddress)}'
        netmask: '{node_netmask}'
        gateway: '{self.hosting_obj.nodes_gateway}'
    - type: nameserver
      address: [{','.join([f"'{dns}'" for dns in self.hosting_obj.nodes_dns_servers])}]
""",
                "user-data": f"""
#cloud-config
hostname: {node_obj.name}
manage_etc_hosts: true
fqdn: {node_obj.name}
user: ekp
ssh_authorized_keys:
  - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIxJN2KSjynrQPBB19tQH1kNeiXNZT+ACDgDZpiY2LRd marc@desktop-marc
chpasswd:
  expire: False
users:
  - default
package_upgrade: false
ekp:
  token: {k3s_token}
  version: '{node_obj.kube_cluster.version}'
  mode: '{k3s_mode}'
  labels: ['topology.kubernetes.io/region={self.hosting_obj.region_name}','topology.kubernetes.io/zone={self.hosting_obj.zone_name}']
  {k3s_master_url}
  {load_balancers_ips}
  {management_ip}
  {services_subnet}
  {pods_subnet}
""",
                "meta-data": "instance-id: {instanceid}",
                "vendor-data": "",
            },
        )
        storage_name = "local"
        cloudinit_image_id = f"{storage_name}:iso/{filename}"
        task_upid = ""
        with open(cloudinit_local_path, "rb") as f:
            ProxmoxApiService(self.proxmox_auth).upload_content(
                node=chosen_pve_node["node"],
                storage=storage_name,
                content="iso",
                filename=f,
            )

        # Set cloud-init disk to vm
        ProxmoxApiService(self.proxmox_auth).set_vm_config(
            node=chosen_pve_node["node"],
            vmid=next_id,
            ide2=f"file={cloudinit_image_id},media=cdrom",
        )
        node_obj.pve_cloudinit_image = cloudinit_image_id

        # Resize disk
        ProxmoxApiService(self.proxmox_auth).resize_vm_disk(
            node=chosen_pve_node["node"],
            vmid=next_id,
            disk="scsi0",
            size="30G",
        )

        # Start vm
        ProxmoxApiService(self.proxmox_auth).start_vm(
            node=chosen_pve_node["node"],
            vmid=next_id,
        )

    def stop_node(self, pve_node, vmid):
        status = ProxmoxApiService(self.proxmox_auth).get_vm_status(
            node=pve_node,
            vmid=vmid,
        )
        if "status" in status and status["status"] == "running":
            ProxmoxApiService(self.proxmox_auth).shutdown_vm(
                node=pve_node,
                vmid=vmid,
            )

    def delete_iso(self, pve_node, image_id):
        storage_match = re.match(r"^([^:]+):.*$", image_id)
        storage_id = storage_match.group(1)
        if storage_id is None:
            raise Exception(f"Unable to parse iso id {image_id}")
        ProxmoxApiService(self.proxmox_auth).delete_content(
            node=pve_node,
            storage=storage_id,
            content_id=image_id,
        )

    def delete_node(self, node_obj):
        try:
            self.stop_node(node_obj.pve_node, node_obj.pve_vmid)
            ProxmoxApiService(self.proxmox_auth).delete_vm(
                node=node_obj.pve_node,
                vmid=node_obj.pve_vmid,
            )
        except ProxmoxApiVmDoesNotExistException:
            pass
        self.delete_iso(node_obj.pve_node, node_obj.pve_cloudinit_image)
