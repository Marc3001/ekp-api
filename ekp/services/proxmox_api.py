from proxmoxer import ProxmoxAPI
from proxmoxer.tools import Tasks


class ProxmoxApiAuth:
    def __init__(self, host, user, token_name, token_value, verify_ssl=False):
        self._proxmox_client = ProxmoxAPI(
            host=host,
            user=user,
            token_name=token_name,
            token_value=token_value,
            verify_ssl=verify_ssl,
        )

    @property
    def proxmox_client(self):
        return self._proxmox_client


class ProxmoxApiException(Exception):
    pass


class ProxmoxApiVmDoesNotExistException(ProxmoxApiException):
    pass


class ProxmoxApiCreateVmException(ProxmoxApiException):
    pass


class ProxmoxApiDeleteVmException(ProxmoxApiException):
    pass


class ProxmoxApiStartVmException(ProxmoxApiException):
    pass


class ProxmoxApiShutdownVmException(ProxmoxApiException):
    pass


class ProxmoxApiResizeVmDiskException(ProxmoxApiException):
    pass


class ProxmoxApiUploadContentException(ProxmoxApiException):
    pass


class ProxmoxApiDeleteContentException(ProxmoxApiException):
    pass


class ProxmoxApiService:
    def __init__(self, client):
        self._proxmox_client = client.proxmox_client

    def list_nodes(self):
        return self._proxmox_client.nodes.get()

    def list_containers(self, node):
        return self._proxmox_client.nodes(node).lxc.get()

    def list_vms(self, node):
        return self._proxmox_client.nodes(node).qemu.get()

    def vm_exists(self, node, vmid):
        return len([v for v in self.list_vms(node) if v["vmid"] == vmid]) > 0

    def create_vm(self, node, **kwargs):
        task_upid = self._proxmox_client.nodes(node).qemu.post(**kwargs)
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiCreateVmException(
                f"Error during vm creation ({kwargs['vmid']}-{kwargs['name']})\n{status['exitstatus']}"
            )

    def delete_vm(self, node, vmid):
        task_upid = self._proxmox_client.nodes(node).qemu(vmid).delete()
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiDeleteVmException(
                f"Error trying to delete vm {vmid}\n{status['exitstatus']}"
            )

    def start_vm(self, node, vmid):
        task_upid = self._proxmox_client.nodes(node).qemu(vmid).status.start.post()
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiStartVmException(
                f"Error starting vm {vmid}\n{status['exitstatus']}"
            )

    def shutdown_vm(self, node, vmid):
        task_upid = (
            self._proxmox_client.nodes(node)
            .qemu(vmid)
            .status.shutdown.post(forceStop=1, timeout=30)
        )
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiShutdownVmException(
                f"Error trying to shutdown vm {vmid}\n{status['exitstatus']}"
            )

    def get_vm_config(self, node, vmid):
        return self._proxmox_client.nodes(node).qemu(vmid).config.get()

    def set_vm_config(self, node, vmid, **kwargs):
        self._proxmox_client.nodes(node).qemu(vmid).config.put(**kwargs)

    def get_vm_cloudinit_dump(self, node, vmid, **kwargs):
        return (
            self._proxmox_client.nodes(node).qemu(vmid).cloudinit.dump().get(**kwargs)
        )

    def get_vm_status(self, node, vmid):
        if not self.vm_exists(node, vmid):
            raise ProxmoxApiVmDoesNotExistException(
                f"Vm {vmid} does not exist on proxmox"
            )
        return self._proxmox_client.nodes(node).qemu(vmid).status.current.get()

    def resize_vm_disk(self, node, vmid, **kwargs):
        task_upid = self._proxmox_client.nodes(node).qemu(vmid).resize.put(**kwargs)
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiResizeVmDiskException(
                f"Error resizing vm disk ({vmid} - {kwargs['disk']} - {kwargs['size']})\n{status['exitstatus']}"
            )

    def upload_content(self, node, storage, **kwargs):
        task_upid = (
            self._proxmox_client.nodes(node).storage(storage).upload.post(**kwargs)
        )
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiUploadContentException(
                f"Error uploading {kwargs['content']} content\n{status['exitstatus']}"
            )

    def delete_content(self, node, storage, content_id):
        task_upid = (
            self._proxmox_client.nodes(node)
            .storage(storage)
            .content(content_id)
            .delete()
        )
        status = Tasks.blocking_status(
            prox=self._proxmox_client, task_id=task_upid, polling_interval=1
        )
        if status["exitstatus"] != "OK":
            raise ProxmoxApiDeleteContentException(
                f"Error trying to delete content {content_id}\n{status['exitstatus']}"
            )
