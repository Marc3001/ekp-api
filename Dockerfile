FROM python:3.12-slim-bookworm

WORKDIR /usr/src/app

RUN apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get clean

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /tmp
RUN pip install -r /tmp/requirements.txt

# copy project
COPY ./ekp .
